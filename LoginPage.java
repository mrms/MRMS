package TEAMPRO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import TEAMPRO.DeleteRecordGUI.ImagePanel;

import java.awt.*; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap; 

public class LoginPage implements ActionListener { 

    private JFrame frame; 
    private JButton loginButton; 
    private JButton resetButton; 
    private JTextField userIDField; 
    private JPasswordField userPasswordField; 
    private JLabel userIDLabel; 
    private JLabel userPasswordLabel; 
    private JLabel messageLabel; 
    private JButton connectDBButton;
    private HashMap<String, String> logininfo; 
    
    public static void main2(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("Image Example");
            frame.setSize(400, 500);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            ImageIcon icon = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\486f7ba29ddb7e85d3b291873e2fa8c6.jpg"); // Replace with the actual path
            JLabel label = new JLabel(icon);
            frame.add(label);
            frame.setVisible(true);
        });
    }

    public LoginPage() { 
        initializeComponents(); 
        setLayout(); 
        setActions(); 
        frame.setVisible(true); 
    } 

    private void initializeComponents() { 
        frame = new JFrame(); 
        loginButton = new JButton("Login"); 
        resetButton = new JButton("Reset"); 
        userIDField = new JTextField(); 
        userPasswordField = new JPasswordField(); 
        userIDLabel = new JLabel("userID:"); 
        userPasswordLabel = new JLabel("password:"); 
        messageLabel = new JLabel(); 
       
        logininfo = new HashMap<>(); 
        logininfo.put("admin", "admin"); 
    }

    private void setLayout() { 
    	 //frame.setSize(1, 6);
    	 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         ImageIcon originalIcon = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\18246203_v987-18a.jpg"); // Replace with the actual path
         Image originalImage = originalIcon.getImage();
         Image resizedImage = originalImage.getScaledInstance(400, 350, Image.SCALE_SMOOTH);
         ImageIcon resizedIcon = new ImageIcon(resizedImage);
         JLabel label = new JLabel(resizedIcon);
 
         frame.setTitle("Login Page"); 
         frame.setLayout(new GridBagLayout()); 
         GridBagConstraints constraints = new GridBagConstraints();
      
         constraints.insets = new Insets(10, 10, 10, 10);

         
  
         userIDLabel.setFont(new Font(null, Font.PLAIN, 15)); 
         constraints.gridx = 0; 
         constraints.gridy = 1; 
         frame.add(userIDLabel, constraints);
  
         userIDField.setFont(new Font(null, Font.PLAIN, 15)); 
         constraints.gridx = 1; 
         constraints.gridwidth = 2; 
         constraints.fill = GridBagConstraints.HORIZONTAL; 
         frame.add(userIDField, constraints);
  
         userPasswordLabel.setFont(new Font(null, Font.PLAIN, 15)); 
         constraints.gridx = 0; 
         constraints.gridy = 2; 
         constraints.gridwidth = 1; 
         constraints.fill = GridBagConstraints.NONE; 
         frame.add(userPasswordLabel, constraints);
  
         userPasswordField.setFont(new Font(null, Font.PLAIN, 15)); 
         constraints.gridx = 1; 
         constraints.gridwidth = 3; 
         constraints.fill = GridBagConstraints.HORIZONTAL; 
         frame.add(userPasswordField, constraints);
  
         loginButton.setFont(new Font(null, Font.PLAIN, 15)); 
         loginButton.setFocusable(false); 
         constraints.gridx = 1; 
         constraints.gridy = 3; 
         constraints.gridwidth = 1; 
         constraints.fill = GridBagConstraints.HORIZONTAL; 
         frame.add(loginButton, constraints);
  
         resetButton.setFont(new Font(null, Font.PLAIN, 15)); 
         resetButton.setFocusable(false); 
         constraints.gridx = 2; 
         frame.add(resetButton, constraints);
         
         
  

         messageLabel.setFont(new Font(null, Font.ITALIC, 15)); 
         constraints.gridx = 0; 
         constraints.gridy = 4; 
         constraints.gridwidth = 3; 
         constraints.fill = GridBagConstraints.HORIZONTAL; 
         frame.add(messageLabel, constraints); 
         constraints.gridx = 0;
         constraints.gridy = 0; 
         frame.add(  label,constraints);

  
         frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
         frame.setSize(600, 600); 
         frame.setLocationRelativeTo(null); 
        
    } 

    private void setActions() { 
        loginButton.addActionListener(this); 
        resetButton.addActionListener(this); 
    } 

    @Override 
    public void actionPerformed(ActionEvent e) { 
        if (e.getSource() == resetButton) { 
            userIDField.setText(""); 
            userPasswordField.setText(""); 
        } 

        if (e.getSource() == loginButton) { 
            String userID = userIDField.getText(); 
            String password = String.valueOf(userPasswordField.getPassword()); 

            if (logininfo.containsKey(userID)) { 
                if (logininfo.get(userID).equals(password)) { 
                    messageLabel.setForeground(Color.green); 
                    messageLabel.setText("Login successful"); 
                    frame.dispose(); 

                    SwingUtilities.invokeLater(() -> new CGUI().main(new String[]{})); 
                } else { 
                    messageLabel.setForeground(Color.red); 
                    messageLabel.setText("Wrong password "); 
                    try {
                        playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\WrongPassword.wav");  
                    } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                        ex1.printStackTrace();
                    }
                } 
            } else { 
                messageLabel.setForeground(Color.red); 
                messageLabel.setText("Username not found"); 
                try {
                    playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\username not found.wav");  
                } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                    ex1.printStackTrace();
                }
            } 
        } 
    } 
  
    public static void main(String[] args) { 
        SwingUtilities.invokeLater(LoginPage::new); 
    } 
    private static void playCustomSound(String soundFilePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        File soundFile = new File(soundFilePath);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
}
}