package TEAMPRO;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints; 
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection; 
import java.sql.PreparedStatement; 
import java.sql.SQLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton; 
import javax.swing.JComponent; 
import javax.swing.JFrame; 
import javax.swing.JLabel; 
import javax.swing.JOptionPane; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
import javax.swing.SwingUtilities;

import TEAMPRO.CGUI.ImagePanel; 

public class DeleteRecordGUI { 
    private static JFrame frame; 
    private static JTextField patientadmnField; 

    public void start() { 
        frame = new JFrame("Delete A Record"); 
        frame.setSize(700, 500); 
        frame.setMaximumSize(new Dimension(400, 300)); 
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
        frame.setLocationRelativeTo(null); 

        JPanel panel = new JPanel(new GridBagLayout()); 
        GridBagConstraints constraints = new GridBagConstraints(); 
        panel.setOpaque(false);
        constraints.insets = new Insets(5, 5, 5, 5); 
        
       
        ImagePanel backgroundPanel = new ImagePanel();
        frame.setContentPane(backgroundPanel);

        JLabel admnLabel = new JLabel("Admission number:"); 
        patientadmnField = new JTextField(); 
        admnLabel.setFont(new Font("Arial", Font.BOLD |Font.ITALIC, 40)); 
        setComponentSize(patientadmnField, 200, 30); 

        JButton deleteButton = new JButton("Delete Record"); 
        deleteButton.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) { 
                try { 
                    deleteRecord(); 
                } catch (Exception e1) { 
                    e1.printStackTrace(); 
                } 
            } 
        }); 

        constraints.gridx = 0; 
        constraints.gridy = 0; 
        panel.add(admnLabel, constraints); 

        constraints.gridx = 1; 
        constraints.gridy = 1; 
        panel.add(deleteButton, constraints); 

        constraints.gridx = 1; 
        constraints.gridy = 0; 
        panel.add(patientadmnField, constraints); 

        frame.add(panel); 
        frame.setVisible(true); 
    } 

    private void setComponentSize(JComponent component, int width, int height) { 
        component.setPreferredSize(new Dimension(width, height)); 
        component.setMaximumSize(new Dimension(width, height)); 
        component.setMinimumSize(new Dimension(width, height)); 
    } 

    public static void deleteRecord() throws Exception { 
        String delete = "DELETE FROM MEDICAL_RECORDS WHERE ADMN_NUMBER=?"; 

        try (Connection connection = DB_CONNECTION.getConnection(); 
             PreparedStatement preparedStatement = connection.prepareStatement(delete)) { 

            int ADMN_NUMBER = Integer.parseInt(patientadmnField.getText()); 
            int dialogResult = JOptionPane.showConfirmDialog(frame, "Are you sure you want to delete this record?", "Confirmation", JOptionPane.YES_NO_OPTION); 
            if (dialogResult == JOptionPane.YES_OPTION) { 
                preparedStatement.setInt(1, ADMN_NUMBER); 
                int affectedRows = preparedStatement.executeUpdate(); 

                if (affectedRows > 0) { 
                    JOptionPane.showMessageDialog(frame, 
                            "Record deleted successfully!", 
                            "Success", 
                            JOptionPane.INFORMATION_MESSAGE); 
                } else { 
                	
                	 try {
                         playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\no record has been found.wav");  
                     } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                         ex1.printStackTrace();
                     }
                    JOptionPane.showMessageDialog(frame, 
                            "No record has been found with the given Admission number", 
                            "Error", 
                            JOptionPane.ERROR_MESSAGE);
                    patientadmnField.setText("");
                    
                    
                } 
            } 
        } catch (SQLException  | NumberFormatException e) {
        	 try {
                 playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Sorry we are not able to add this record could you check it again.wav");  
             } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                 ex1.printStackTrace();
             }
        	JOptionPane.showMessageDialog(frame,
                    "Sorry, we are not able to find this record. Please check again.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        	patientadmnField.setText("");
            e.printStackTrace();
    } }

    public static void main(String[] args) { 
        SwingUtilities.invokeLater(new Runnable() { 
            @Override 
            public void run() { 
                new DeleteRecordGUI().start(); 
            } 
        }); 
    } 
    private static void playCustomSound(String soundFilePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        File soundFile = new File(soundFilePath);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
}
    
    public static class ImagePanel extends JPanel{
    	private Image backgroundImage;
    	
    	public ImagePanel() {
    		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\Screenshot (354).png").getImage();
    		backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\DeleteBackgroung.gif").getImage();
    		
    		
    	}
    	@Override
    	protected void paintComponent(Graphics g) {
    		super.paintComponent(g);
    		g.drawImage(backgroundImage,0,0,getWidth(),getHeight(),this);
    	}
    }
    }
