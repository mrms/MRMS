package TEAMPRO;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import TEAMPRO.CGUI.ImagePanel;
 
public class DisplayRecordGUI {
    private static JFrame frame;
    private static JTextField patientadmnField;
    private static JTextArea detailsArea;
 
    public void start() {
        frame = new JFrame("Display A Record");
        frame.setSize(700, 500);
      
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
 
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        panel.setOpaque(false);
        constraints.insets = new Insets(5, 5, 5, 5);
        
        ImagePanel backgroundPanel = new ImagePanel();
        frame.setContentPane(backgroundPanel);
 
        JLabel admnLabel = new JLabel("Admission number:");
        patientadmnField = new JTextField();
        admnLabel.setFont(new Font("Arial", Font.BOLD |Font.ITALIC, 40)); 
        //admnLabel.setForeground(Color.WHITE);
        setComponentSize(patientadmnField, 250, 30);
 
        JButton displayButton = new JButton("Display Record");
        displayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    displayRecords();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
 
        detailsArea = new JTextArea();
        detailsArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(detailsArea);
        scrollPane.setPreferredSize(new Dimension(400, 300));
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
 
        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(admnLabel, constraints);
 
        constraints.gridx = 1;
        constraints.gridy = 0;
        panel.add(patientadmnField, constraints);
 
        constraints.gridx = 1;
        constraints.gridy = 1;
        panel.add(displayButton, constraints);
 
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        panel.add(scrollPane, constraints);
 
        frame.add(panel);
        frame.setVisible(true);
    }
 
    private void setComponentSize(JComponent component, int width, int height) {
        component.setPreferredSize(new Dimension(width, height));
        component.setMaximumSize(new Dimension(width, height));
        component.setMinimumSize(new Dimension(width, height));
    }
 
    public static void displayRecords() throws Exception {
        String Display = "SELECT * FROM MEDICAL_RECORDS WHERE ADMN_NUMBER=?";
 
        try (Connection connection = DB_CONNECTION.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(Display)) {
 
            int ADMN_NUMBER = Integer.parseInt(patientadmnField.getText());
            preparedStatement.setInt(1, ADMN_NUMBER);
 
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                StringBuilder records = new StringBuilder();
 
                if (resultSet.next()) {
                    do {
                        int adminNumber = resultSet.getInt("ADMN_NUMBER");
                        String NAME = resultSet.getString("NAME");
                        int AGE = resultSet.getInt("AGE");
                        String GENDER = resultSet.getString("GENDER");
                        String DATE_OF_BIRTH = resultSet.getString("DATE_OF_BIRTH");
                        int HEIGHT = resultSet.getInt("HEIGHT");
                        int WEIGHT = resultSet.getInt("WEIGHT");
                        String BLOOD_GROUP = resultSet.getString("BLOOD_GROUP");
                        String COVID_STATUS = resultSet.getString("COVID_STATUS");
                        String COVID_VACCINATION_STATUS = resultSet.getString("COVID_VACCINATION_STATUS");
                        String PAST_HISTORY = resultSet.getString("PAST_HISTORY");
                        String MAJOR_ILLNESS = resultSet.getString("MAJOR_ILLNESS");
                        String IMPLANTS = resultSet.getString("IMPLANTS");
 
                        records.append("Admin Number: ").append(adminNumber)
                                .append("\nName: ").append(NAME)
                                .append("\nAge: ").append(AGE)
                                .append("\nGender: ").append(GENDER)
                                .append("\nDate of Birth: ").append(DATE_OF_BIRTH)
                                .append("\nHeight: ").append(HEIGHT)
                                .append("\nWeight: ").append(WEIGHT)
                                .append("\nBlood Group: ").append(BLOOD_GROUP)
                                .append("\nCOVID Status: ").append(COVID_STATUS)
                                .append("\nCOVID Vaccination Status: ").append(COVID_VACCINATION_STATUS)
                                .append("\nPast History: ").append(PAST_HISTORY)
                                .append("\nMajor Illness: ").append(MAJOR_ILLNESS)
                                .append("\nImplants: ").append(IMPLANTS).append("\n\n");
                    } while (resultSet.next());
                    detailsArea.setText(records.toString());
                } else {
                	 try {
                         playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Admission number doesn't exist in the database..wav");  
                     } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                         ex1.printStackTrace();
                     }
                	 
                    JOptionPane.showMessageDialog(frame,
                            "Admission number doesn't exist in the database.",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                    patientadmnField.setText("");
                }
            }
        } catch (SQLException | NumberFormatException e) {
        	 try {
                 playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Sorry, we are not able to find this record. Please check again..wav");  
             } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                 ex1.printStackTrace();
             }
            JOptionPane.showMessageDialog(frame,
                    "Sorry, we are not able to find this record. Please check again.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            patientadmnField.setText("");
            e.printStackTrace();
        }
    }
    private static void playCustomSound(String soundFilePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        File soundFile = new File(soundFilePath);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
} 
 
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new DisplayRecordGUI().start();
            }
        });
    }
    static class ImagePanel extends JPanel{
    	private Image backgroundImage;
    	
    	public ImagePanel() {
    		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\Screenshot (354).png").getImage();
    		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\1740337_7373.jpg").getImage();
    		backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Gold Wallpaper Plain.jfif").getImage();
    		
    		
    		
    	}
    	@Override
    	protected void paintComponent(Graphics g) {
    		super.paintComponent(g);
    		g.drawImage(backgroundImage,0,0,getWidth(),getHeight(),this);
    	}
    }
}