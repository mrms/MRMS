package TEAMPRO;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import TEAMPRO.DeleteRecordGUI.ImagePanel;

import java.awt.*; 

import java.awt.event.ActionEvent; 

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection; 

import java.sql.PreparedStatement; 

import java.sql.SQLException; 

  

import javax.swing.*;  

import java.awt.*;  

import java.awt.event.ActionEvent;  

import java.awt.event.ActionListener;  

import java.sql.Connection;  

import java.sql.PreparedStatement;  

import java.sql.SQLException;  

import java.sql.ResultSet; 

public class ModifyRecordGUI {  
    private JFrame frame;  
    private JTextField patientadmnField;  
    private JTextField patientNameField;  
    private JTextField ageField;  
    private JComboBox<String> genderComboBox;  
    private JComboBox<String> DayComboBox;  
    private JComboBox<String> MonthComboBox;  
    private JComboBox<String> YearComboBox;  
    private JTextField patientHeightField;  
    private JTextField patientWeightField;  
    private JComboBox<String> BloodGroupComboBox;  
    private JRadioButton RadioButton1;  
    private JRadioButton RadioButton2;  
    private JRadioButton RadioButton3;  
    private JRadioButton RadioButton4;  
    private JCheckBox CheckButton1;  
    private JCheckBox CheckButton2;  
    private JCheckBox CheckButton3;  
    private JCheckBox CheckButton4;  
    private JTextField OtherField;  
    private JCheckBox CheckButton5;  
    private JCheckBox CheckButton6;  
    private JCheckBox CheckButton7;  
    private JCheckBox CheckButton8;  
    private JTextField OtherField2;  
    private JButton checkAdmissionButton; 
    public void start() {  
        frame = new JFrame("Modify A Record");  
        frame.setSize(850,800);  
        frame.setMaximumSize(new Dimension(400, 300));  
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
        frame.setLocationRelativeTo(null);  
        JPanel panel = new JPanel(new GridBagLayout());  
        GridBagConstraints constraints = new GridBagConstraints();  
        GridBagConstraints constraints1 = new GridBagConstraints();
        panel.setOpaque(false);
        constraints.insets = new Insets(5, 5, 5, 5);  
        JLabel admnLabel = new JLabel("Admission number:"); 
        admnLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        patientadmnField = new JTextField();  
        setComponentSize(patientadmnField, 200, 30); 
        
        ImagePanel backgroundPanel = new ImagePanel();
        frame.setContentPane(backgroundPanel);


        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        patientNameField = new JTextField();  
        setComponentSize(patientNameField, 200, 30);  

        JLabel ageLabel = new JLabel("Age:");  
        ageLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        ageField = new JTextField();  
        setComponentSize(ageField, 200, 30);  

        JLabel genderLabel = new JLabel("Gender:"); 
        genderLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        String[] genders = {"Male", "Female", "Other"};  
        genderComboBox = new JComboBox<>(genders);  
        setComponentSize(genderComboBox, 150, 30); 

        JLabel DayLabel = new JLabel("Date of birth:");  
        DayLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        String[] days = new String[31];  
        for (int i = 0; i < 31; i++) {  
            days[i] = String.valueOf(i + 1);  
        }  
        DayComboBox = new JComboBox<>(days);  
        setComponentSize(DayComboBox, 50, 30);  
        String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};  
        MonthComboBox = new JComboBox<>(months);  
        setComponentSize(MonthComboBox, 70, 30);  
        String[] years = new String[100];  
        for (int i = 1950; i < 2050; i++) {  
            years[i - 1950] = String.valueOf(i);  
        }  
        YearComboBox = new JComboBox<>(years);  
        setComponentSize(YearComboBox, 80, 30);  

        JLabel heightLabel = new JLabel("Height:");  
        heightLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        patientHeightField = new JTextField();  
        setComponentSize(patientHeightField, 200, 30);  

        JLabel weightLabel = new JLabel("Weight:");  
        weightLabel.setFont(new Font(null,  Font.BOLD | Font.ITALIC, 15)); 
        patientWeightField = new JTextField();  
        setComponentSize(patientWeightField, 200, 30);  

        JButton calculateBMIButton = new JButton("Find BMI");  
        calculateBMIButton.addActionListener(new ActionListener() {  
            @Override  
            public void actionPerformed(ActionEvent e) {  
                calculateBMI();  
            }  
        });  

        JLabel BloodGroupLabel = new JLabel("BloodGroup:");  
        BloodGroupLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        String[] BloodGroup = {"O+","O-","A+","A-","B+","B-","AB+","AB-","H+"};  
        BloodGroupComboBox = new JComboBox<>(BloodGroup);  
        setComponentSize(BloodGroupComboBox, 150, 30);  

        JLabel CovidStatusLabel = new JLabel("Covid Status:");  
        CovidStatusLabel.setFont(new Font(null,  Font.BOLD | Font.ITALIC, 15)); 
        RadioButton1 = new JRadioButton("Yes");  
        RadioButton2 = new JRadioButton("No");  
        ButtonGroup covidStatusGroup = new ButtonGroup();  
        covidStatusGroup.add(RadioButton1);  
        covidStatusGroup.add(RadioButton2);  

        JLabel CovidVacStatusLabel = new JLabel("Covid vaccination Status:"); 
        CovidVacStatusLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        RadioButton3 = new JRadioButton("Yes");  
        RadioButton4 = new JRadioButton("No");  
        ButtonGroup covidVacStatusGroup = new ButtonGroup();  
        covidVacStatusGroup.add(RadioButton3);  
        covidVacStatusGroup.add(RadioButton4); 
        JLabel PastHistoryLabel = new JLabel("Past History"); 
        PastHistoryLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        
        CheckButton1 = new JCheckBox("Jaundice");  
        setComponentSize(CheckButton1, 200, 30);  
        CheckButton2 = new JCheckBox("Allergies");  
        setComponentSize(CheckButton1, 200, 30);  
        CheckButton3 = new JCheckBox("Blood Transfusion");  
        setComponentSize(CheckButton1, 200, 30);  
        CheckButton4 = new JCheckBox("NIL");  
        setComponentSize(CheckButton1, 200, 30);  
        JLabel OtherLabel = new JLabel("Any major illness/operations in the past");  
        OtherLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        OtherField = new JTextField();  
        setComponentSize(OtherField,200,30);  

        JLabel ImplantsLabel = new JLabel("Using any Implants"); 
        ImplantsLabel.setFont(new Font(null, Font.BOLD | Font.ITALIC, 15)); 
        CheckButton5 = new JCheckBox("Dental Implant");  
        setComponentSize(CheckButton5, 200, 30);  
        CheckButton6 = new JCheckBox("Braces");  
        setComponentSize(CheckButton6, 200, 30);  
        CheckButton7 = new JCheckBox("Spectacles");  
        setComponentSize(CheckButton7, 200, 30);  
        CheckButton8 = new JCheckBox("NIL");  
        setComponentSize(CheckButton8, 200, 30);  
        OtherField2 = new JTextField();  
        setComponentSize(OtherField2,200,30);  

        checkAdmissionButton = new JButton("Check Admission"); 
        checkAdmissionButton.addActionListener(new ActionListener() { 
            @Override 
            public void actionPerformed(ActionEvent e) { 
                try { 
checkAdmission(); 
} catch (Exception e1) { 
// TODO Auto-generated catch block 
e1.printStackTrace(); 
} 
            } 
        }); 

        JButton addButton = new JButton("Modify Record");  
        addButton.addActionListener(new ActionListener() {  
            @Override  
            public void actionPerformed(ActionEvent e) {  
                try {  
addRecord();  
} catch (Exception e1) {  
// TODO Auto-generated catch block  
e1.printStackTrace();  
}  
            }  
        }); 

        JButton exitButton = new JButton("EXIT");  
        exitButton.addActionListener(new ActionListener() {  
            @Override  
            public void actionPerformed(ActionEvent e) {  
                frame.dispose();  
            }  
        });  

        setComponentSize(addButton, 150, 30);  
        setComponentSize(exitButton, 150, 30); 
        constraints.gridx = 0;  
        constraints.gridy = 0;  
        panel.add(admnLabel, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 0;  
        panel.add(patientadmnField, constraints);  

        constraints.gridx = 2; 
        constraints.gridy = 0; 
        panel.add(checkAdmissionButton, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 1;  
        panel.add(nameLabel, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 1;  
        panel.add(patientNameField, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 2;  
        panel.add(ageLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 2;  
        panel.add(ageField, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 3;  
        panel.add(genderLabel, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 3;  
        panel.add(genderComboBox, constraints);  

        constraints.gridx = 0;  
        constraints.gridy = 4;  
        panel.add(DayLabel, constraints); 

        constraints1.gridx = 1;  
        constraints1.gridy = 4;  
        panel.add(DayComboBox,  constraints1);  

        constraints.gridx = 2;  
        constraints.gridy = 4;  
        panel.add(MonthComboBox, constraints);  

        constraints.gridx = 3;  
        constraints.gridy = 4;  
        panel.add(YearComboBox, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 5;  
        panel.add(heightLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 5;  
        panel.add(patientHeightField, constraints);  

        constraints.gridx = 0;  
        constraints.gridy = 6;  
        panel.add(weightLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 6;  
        panel.add(patientWeightField, constraints); 

        constraints.gridx = 2;  
        constraints.gridy = 6;  
        panel.add(calculateBMIButton, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 7;  
        panel.add(BloodGroupLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 7;  
        panel.add(BloodGroupComboBox, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 8;  
        panel.add(CovidStatusLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 8;  
        panel.add(RadioButton1, constraints);  

        constraints.gridx = 2;  
        constraints.gridy = 8;  
        panel.add(RadioButton2, constraints);  

        constraints.gridx = 0;  
        constraints.gridy = 9;  
        panel.add(CovidVacStatusLabel, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 9;  
        panel.add(RadioButton3, constraints);  

        constraints.gridx = 2;  
        constraints.gridy = 9; 
        panel.add(RadioButton4, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 10;  
        panel.add(PastHistoryLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 10;  
        panel.add(CheckButton1, constraints);  

        constraints.gridx = 2;  
        constraints.gridy = 10;  
        panel.add(CheckButton2, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 11;  
        panel.add(CheckButton3, constraints); 

        constraints.gridx = 2;  
        constraints.gridy = 11;  
        panel.add(CheckButton4, constraints);  

        constraints.gridx = 0;  
        constraints.gridy = 12;  
        panel.add(OtherLabel, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 12;  
        panel.add(OtherField, constraints); 

        constraints.gridx = 0;  
        constraints.gridy = 13;  
        panel.add(ImplantsLabel, constraints); 

        constraints.gridx = 1;  
        constraints.gridy = 13;  
        panel.add(CheckButton5, constraints);  

        constraints.gridx = 2;  
        constraints.gridy = 13; 
        panel.add(CheckButton6, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 14;  
        panel.add(CheckButton7, constraints); 

        constraints.gridx = 2;  
        constraints.gridy = 14;  
        panel.add(CheckButton8, constraints);  

        constraints.gridx = 1;  
        constraints.gridy = 15;  
        panel.add(OtherField2, constraints);  

        constraints.gridx = 0;  
        constraints.gridy = 16;  
        panel.add(addButton, constraints);  

        constraints.gridx = 2;  
        constraints.gridy = 16;  
        panel.add(exitButton, constraints);  

        frame.add(panel);  
        frame.setVisible(true);  
    }  


    private void setComponentSize(JComponent component, int width, int height) {  
        component.setPreferredSize(new Dimension(width, height));  
        component.setMaximumSize(new Dimension(width, height));  
        component.setMinimumSize(new Dimension(width, height));  
    }  
    private void checkAdmission() throws Exception { 
        try { 
            int ADMN = Integer.parseInt(patientadmnField.getText()); 
            String CHECK_EXISTENCE_QUERY = "SELECT * FROM MEDICAL_RECORDS WHERE ADMN_NUMBER=?"; 
            Connection connection = DB_CONNECTION.getConnection(); 
            PreparedStatement preparedStatement = connection.prepareStatement(CHECK_EXISTENCE_QUERY); 
            preparedStatement.setInt(1, ADMN); 
            ResultSet resultSet = preparedStatement.executeQuery(); 
            if (resultSet.next()) { 
            	 try {
                     playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Admission number exists..wav");  
                 } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                     ex1.printStackTrace();
                 }
            	
                JOptionPane.showMessageDialog(frame, "Admission number exists.", "Existence Check", JOptionPane.INFORMATION_MESSAGE); 
            } else { 
            	 try {
                     playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Admission number does not exist..wav");  
                 } catch (IOException | UnsupportedAudioFileException | LineUnavailableException|NumberFormatException ex1) {
                     ex1.printStackTrace();
                 }
                JOptionPane.showMessageDialog(frame, "Admission number does not exist.", "Existence Check", JOptionPane.WARNING_MESSAGE); 
            } 

            resultSet.close(); 
            preparedStatement.close(); 
            connection.close(); 
        } catch (SQLException | NumberFormatException e) { 
        	 try {
                 playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Enter a Valid Admission Number.wav");  
             } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                 ex1.printStackTrace();
             }
            e.printStackTrace(); 
            JOptionPane.showMessageDialog(frame, "Enter a Valid Admission Number", "Error", JOptionPane.ERROR_MESSAGE); 
        } 
    } 
    private static void playCustomSound(String soundFilePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        File soundFile = new File(soundFilePath);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
} 

    private void addRecord() throws Exception {  

        try {  
            int ADMN = Integer.parseInt(patientadmnField.getText());  
            String Name = patientNameField.getText();  
            int Age = Integer.parseInt(ageField.getText());  
            String Gender = (String) genderComboBox.getSelectedItem();  
            String day = (String) DayComboBox.getSelectedItem();  
            String month = (String) MonthComboBox.getSelectedItem();  
            String year = (String) YearComboBox.getSelectedItem();  
            String Birthday = day+" "+month+" "+year;  
            int Height = Integer.parseInt(patientHeightField.getText());  
            int Weight = Integer.parseInt(patientWeightField.getText());  
            String BloodGroup = (String) BloodGroupComboBox.getSelectedItem();  
            String CovidStatus = (RadioButton1.isSelected())?"Yes":"No";  
            String CovidVacStatus = (RadioButton3.isSelected())?"Yes":"No";  
            String Jaundice = (CheckButton1.isSelected())?"Jaundice":"";  
            String Allergies = (CheckButton2.isSelected())?"Allergies":"";  
            String BloodTransfusion = (CheckButton3.isSelected())?"BloodTransfusion":"";  
            String Nil = (CheckButton4.isSelected())?"Nil":"";  
            String Other = OtherField.getText();  
            String pasthistories =  Jaundice +" "+ Allergies +" "+ BloodTransfusion +" "+ Nil;  
            String DentalImplant = (CheckButton5.isSelected())?"Dental Implant":"";  
            String Braces = (CheckButton6.isSelected())?"Braces":"";  
            String Spectacles = (CheckButton7.isSelected())?"Spectacles":"";  
            String Nil2 = (CheckButton8.isSelected())?"Nil":"";  
            String Other2 = OtherField2.getText();  
            String implants = DentalImplant +" "+ Braces +" "+ Spectacles +" "+ Nil2+" "+Other2;  

            int confirm = JOptionPane.showConfirmDialog(  
                    frame,"Are you sure want to Modify this record"+  
                    		JOptionPane.YES_NO_OPTION);  
            if (confirm == JOptionPane.YES_OPTION) {  
            	String UPDATE_RECORD = "UPDATE MEDICAL_RECORDS SET NAME=?, AGE=?, GENDER=?, DATE_OF_BIRTH=?, "  
                        + "HEIGHT=?, WEIGHT=?, BLOOD_GROUP=?, COVID_STATUS=?, COVID_VACCINATION_STATUS=?, "  
                        + "PAST_HISTORY=?, MAJOR_ILLNESS=?, IMPLANTS=? WHERE ADMN_NUMBER=?";  
            	Connection connection = DB_CONNECTION.getConnection();   
  	             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_RECORD);  
  	           preparedStatement.setString(1, Name);  
  	         preparedStatement.setInt(2, Age);  
  	         preparedStatement.setString(3, Gender);  
  	         preparedStatement.setString(4, Birthday);  
  	         preparedStatement.setInt(5, Height);  
  	         preparedStatement.setInt(6, Weight);  
  	         preparedStatement.setString(7, BloodGroup);  
  	         preparedStatement.setString(8, CovidStatus);  
  	         preparedStatement.setString(9, CovidVacStatus);  
  	         preparedStatement.setString(10, pasthistories);  
  	         preparedStatement.setString(11, Other2);  
  	         preparedStatement.setString(12, implants);  
  	         preparedStatement.setInt(13, ADMN);  
  	       int rowsAffected = preparedStatement.executeUpdate();
  	     
           if (rowsAffected > 0) {
               JOptionPane.showMessageDialog(frame, "Record updated successfully!");
           } else {
               JOptionPane.showMessageDialog(frame, "Record not found or update failed.",
                       "Error", JOptionPane.ERROR_MESSAGE);
           }
          }}catch (SQLException e) {
           JOptionPane.showMessageDialog(frame, "Error updating record: " + e.getMessage(),
                   "Error", JOptionPane.ERROR_MESSAGE);
       } catch (NumberFormatException e) {
    	   try {
               playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Sorry we are not able to add this record could you check it again.wav");  
           } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
               ex1.printStackTrace();
           }
    	   
   JOptionPane.showMessageDialog(frame,
           "Sorry we are not able to modify this record could you check it again",
           "Error", JOptionPane.ERROR_MESSAGE);
}}
   
    private void calculateBMI() {  
        try {  
            double height = Double.parseDouble(patientHeightField.getText()) / 100.0;   
            double weight = Double.parseDouble(patientWeightField.getText()); 
            double bmi = weight / (height * height);  
            String bmiCategory;  
            if (bmi < 18.5) {  
                bmiCategory = "Underweight";  
            } else if (bmi > 18.5 && bmi < 24.9) {  
                bmiCategory = "Normal weight";  
            } else if (bmi > 24.9 && bmi < 29.9) {  
                bmiCategory = "Overweight";  
            } else {  
                bmiCategory = "Obese";  
            } 
            JOptionPane.showMessageDialog(frame,  
                    "BMI: " + String.format("%.2f", bmi) + "\nCategory:  " + bmiCategory,  
                    "BMI Calculation",  
                    JOptionPane.INFORMATION_MESSAGE);  
        } catch (NumberFormatException e) {  
        	 try {
                 playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Invalid height or weight. Please enter numeric values..wav");  
             } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                 ex1.printStackTrace();
             }
            JOptionPane.showMessageDialog(frame,  
                    "Invalid height or weight. Please enter numeric values.",  
                    "Error",  
                    JOptionPane.ERROR_MESSAGE);  
        }  
    }  
            public static void main(String[] args) {  
                SwingUtilities.invokeLater(new Runnable() {  
                    @Override  
                    public void run() {  
                        new ModifyRecordGUI().start();  
                    }  
                });  
        }  
            static class ImagePanel extends JPanel{
            	private Image backgroundImage;
            	
            	public ImagePanel() {
            		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\Screenshot (354).png").getImage();
            		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\abstract-empty-smooth-light-pink-studio-room-background-use-as-montage-product-display-banner-template.jpg").getImage();
            		backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Gold Wallpaper Plain.jfif").getImage();
            		
            	}
            	@Override
            	protected void paintComponent(Graphics g) {
            		super.paintComponent(g);
            		g.drawImage(backgroundImage,0,0,getWidth(),getHeight(),this);
            	}
            }
        } 