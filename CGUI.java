package TEAMPRO;
import javax.swing.*; 
import java.awt.*; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 

public class CGUI { 
    private static JFrame main_window; 
    

    public static void main(String[] args) { 
    	
    	
    	
        main_window = new JFrame(); 
        main_window.setTitle("Medical Records Maintenance"); 

        ImagePanel backgroundPanel = new ImagePanel();
        main_window.setContentPane(backgroundPanel);
        
        JPanel panel = new JPanel(); 
        panel.setLayout(new GridBagLayout()); 
        GridBagConstraints gbc = new GridBagConstraints(); 
        panel.setOpaque(false);
        panel.setBackground(Color.WHITE); 

        JLabel llab = new JLabel("Medical Records Maintenance"); 
        llab.setFont(new Font("Arial", Font.BOLD, 30)); 
        gbc.gridx = 0; 
        gbc.gridy = 0; 
        gbc.gridwidth = 2; 
        gbc.insets = new Insets(10, 10, 10, 10); 
        gbc.anchor = GridBagConstraints.CENTER; 
        panel.add(llab, gbc); 

        JButton btn1 = createButton("ADD NEW RECORD"); 
        JButton btn2 = createButton("DISPLAY RECORD"); 
        JButton btn3 = createButton("MODIFY RECORD"); 
        JButton btn4 = createButton("DELETE RECORD"); 
        JButton btn5 = createButton("SHOW ALL RECORD"); 
        JButton btn6 = createButton("EXIT"); 

        gbc.gridwidth = 1; 
        gbc.anchor = GridBagConstraints.CENTER; 

        gbc.gridy = 1; 
        gbc.gridx = 1; 
        panel.add(btn1, gbc); 

        gbc.gridy = 2; 
        panel.add(btn2, gbc); 

        gbc.gridy = 3; 
        panel.add(btn3, gbc); 

        gbc.gridy = 4; 
        panel.add(btn4, gbc); 

        gbc.gridy = 5; 
        panel.add(btn5, gbc); 

        gbc.gridy = 6; 
        panel.add(btn6, gbc); 

        main_window.add(panel); 

        main_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        main_window.pack(); 
        main_window.setLocationRelativeTo(null); 
        main_window.setVisible(true); 
    } 

    private static JButton createButton(String text) { 
        JButton button = new JButton(text); 
        button.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) { 
                handleButtonClick(text); 
            } 
        }); 
        button.setBackground(Color.GRAY); 
        button.setPreferredSize(new Dimension(200, 40)); 
        button.setAlignmentX(Component.CENTER_ALIGNMENT); 
       
        Font buttonFont = button.getFont(); 
        button.setFont(new Font(buttonFont.getName(), Font.PLAIN, 16)); 

        return button; 
    } 

    private static void handleButtonClick(String buttonName) { 
        switch (buttonName) { 
            case "ADD NEW RECORD": 
                SwingUtilities.invokeLater(() -> new AddNewRecordGUI().start()); 
                break; 
            case "DISPLAY RECORD": 
            	SwingUtilities.invokeLater(() -> new DisplayRecordGUI().start()); 
                break; 
            case "MODIFY RECORD": 
            	SwingUtilities.invokeLater(() -> new ModifyRecordGUI().start()); 
                break; 
            case "DELETE RECORD": 
            	SwingUtilities.invokeLater(() -> new DeleteRecordGUI().start()); 
                break; 
            case "SHOW ALL RECORD": 
            	SwingUtilities.invokeLater(() -> new ShowAllRecordsGUI().start()); 
            	break; 
            case "EXIT": 
                closeWindow(); 
                break; 
        } 
    } 

    private static void closeWindow() { 
        main_window.dispose(); 
    }
    static class ImagePanel extends JPanel{
    	private Image backgroundImage;
    	
    	public ImagePanel() {
    		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\Screenshot (354).png").getImage();
    		backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\CGUIBackground.jpg").getImage();
    		
    		
    	}
    	@Override
    	protected void paintComponent(Graphics g) {
    		super.paintComponent(g);
    		g.drawImage(backgroundImage,0,0,getWidth(),getHeight(),this);
    	}
    }
} 
