package TEAMPRO;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import TEAMPRO.DeleteRecordGUI.ImagePanel;

import java.awt.*; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet; 
import java.sql.SQLException; 
  
public class ShowAllRecordsGUI { 
    private JFrame frame; 
    private static JPasswordField passwordField; 
    private static JTextField patientadmnField; 
    private static JTextArea detailsArea; 


    public void start() { 
        frame = new JFrame("Show All Records"); 
        frame.setSize(700, 500); 
        frame.setMaximumSize(new Dimension(400, 300)); 
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
        frame.setLocationRelativeTo(null); 

        JPanel panel = new JPanel(new GridBagLayout()); 
        GridBagConstraints constraints = new GridBagConstraints(); 
        panel.setOpaque(false);
        constraints.insets = new Insets(5, 5, 5, 5); 
        
        ImagePanel backgroundPanel = new ImagePanel();
        frame.setContentPane(backgroundPanel);


        JLabel passwordLabel = new JLabel("Enter Password:"); 
        passwordField = new JPasswordField(); 
        passwordLabel.setFont(new Font("Arial", Font.BOLD |Font.ITALIC, 40)); 
        setComponentSize(passwordField, 200, 30); 

        JButton passwordButton = new JButton("Enter"); 
        passwordButton.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent e) { 
                char[] enteredPassword = passwordField.getPassword(); 
                if (isValidPassword(enteredPassword)) { 
                    try { 
                    	Toolkit.getDefaultToolkit().beep();
displayRecords(); 
} catch (Exception e1) { 
// TODO Auto-generated catch block 
e1.printStackTrace(); 
} 
                } else { 
                	 try {
                         playCustomSound("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\WrongPassword.wav");  
                     } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ex1) {
                         ex1.printStackTrace();
                     }
                    JOptionPane.showMessageDialog(frame, "Wrong Password", "Error", JOptionPane.ERROR_MESSAGE); 
                }            
                passwordField.setText(""); 
            } 
        }); 

        detailsArea = new JTextArea(); 
        detailsArea.setEditable(false); 
        JScrollPane scrollPane = new JScrollPane(detailsArea); 
        scrollPane.setPreferredSize(new Dimension(400, 300)); 
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 

        constraints.gridx = 0; 
        constraints.gridy = 0; 
        panel.add(passwordLabel, constraints); 

        constraints.gridx = 1; 
        constraints.gridy = 0; 
        panel.add(passwordField, constraints); 

        constraints.gridx = 2; 
        constraints.gridy = 0; 
        panel.add(passwordButton, constraints); 

        constraints.gridx = 0; 
        constraints.gridy = 1; 
        constraints.gridwidth = 3; 
        panel.add(scrollPane, constraints); 

        frame.add(panel); 
        frame.setVisible(true); 
    } 

    private void setComponentSize(JComponent component, int width, int height) { 
        component.setPreferredSize(new Dimension(width, height)); 
        component.setMaximumSize(new Dimension(width, height)); 
        component.setMinimumSize(new Dimension(width, height)); 
    } 

    private boolean isValidPassword(char[] enteredPassword) { 
    
        char[] correctPassword = "SYSTEM".toCharArray(); 
        return java.util.Arrays.equals(enteredPassword, correctPassword); 
    } 

    public static void displayRecords() throws Exception { 
        String SELECT_ALL_RECORDS = "SELECT * FROM MEDICAL_RECORDS"; 
        try (Connection connection = DB_CONNECTION.getConnection(); 
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_RECORDS); 
             ResultSet resultSet = preparedStatement.executeQuery()) { 

            StringBuilder recordsText = new StringBuilder(); 
            while (resultSet.next()) { 
                int ADMN_NUMBER = resultSet.getInt("ADMN_NUMBER"); 
                String NAME = resultSet.getString("NAME"); 
                int AGE = resultSet.getInt("AGE"); 
                String GENDER = resultSet.getString("GENDER"); 
                String DATE_OF_BIRTH = resultSet.getString("DATE_OF_BIRTH"); 
                int HEIGHT = resultSet.getInt("HEIGHT"); 
                int WEIGHT = resultSet.getInt("WEIGHT"); 
                String BLOOD_GROUP = resultSet.getString("BLOOD_GROUP"); 
                String COVID_STATUS = resultSet.getString("COVID_STATUS"); 
                String COVID_VACCINATION_STATUS = resultSet.getString("COVID_VACCINATION_STATUS"); 
                String PAST_HISTORY = resultSet.getString("PAST_HISTORY"); 
                String MAJOR_ILLNESS = resultSet.getString("MAJOR_ILLNESS"); 
                String IMPLANTS = resultSet.getString("IMPLANTS"); 

                recordsText.append("Admin Number: ").append(ADMN_NUMBER) 
                        .append("\nName: ").append(NAME) 
                        .append("\nAge: ").append(AGE) 
                        .append("\nGender").append(GENDER) 
                        .append("\nDate Of Birthday").append(DATE_OF_BIRTH) 
                        .append("\nHeight").append(HEIGHT) 
                        .append("\nWeight").append(WEIGHT) 
                        .append("\nBlood Group").append(BLOOD_GROUP) 
                        .append("\nCovid Status").append(COVID_STATUS) 
                        .append("\nCovid Vaccination Status").append(COVID_VACCINATION_STATUS) 
                        .append("\nPast History").append(PAST_HISTORY) 
                        .append("\nMajor Illness").append(MAJOR_ILLNESS) 
                        .append("\nImplants").append(IMPLANTS) 
                        .append("\n----------------------------------\n"); 
            } 

            detailsArea.setText(recordsText.toString()); 
        } catch (SQLException e) { 
            e.printStackTrace(); 
        } 
    }  private static void playCustomSound(String soundFilePath) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        File soundFile = new File(soundFilePath);
        AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
        Clip clip = AudioSystem.getClip();
        clip.open(audioIn);
        clip.start();
} 

    public static void main(String[] args) { 
        SwingUtilities.invokeLater(new Runnable() { 
            @Override 
            public void run() { 
               new ShowAllRecordsGUI().start(); 
            } 
        }); 
   } 
    static class ImagePanel extends JPanel{
    	private Image backgroundImage;
    	
    	public ImagePanel() {
    		//backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Downloads\\Screenshot (354).png").getImage();
    		backgroundImage = new ImageIcon("C:\\Users\\sridharshan.d\\Desktop\\MRMS\\Gold Wallpaper Plain.jfif").getImage();
    		
    		
    	}
    	@Override
    	protected void paintComponent(Graphics g) {
    		super.paintComponent(g);
    		g.drawImage(backgroundImage,0,0,getWidth(),getHeight(),this);
    	}
    }
} 